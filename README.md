# Autoinstall Ubuntu 22.04: Puget Solution

## Introduction

We'll follow an article about it: https://www.pugetsystems.com/labs/hpc/ubuntu-22-04-server-autoinstall-iso/.

## Prerequisites:

### Packages
`p7zip`, `wget`, `xorriso`.

### Code

```sh
sudo apt install p7zip &&
sudo apt install wget &&
sudo apt install xorriso
```

> `p7zip` is optional, you can use `tar`.

## Step 1: Downloading

```sh
mkdir u22.04-autoinstall-ISO
cd u22.04-autoinstall-ISO
mkdir source-files
wget https://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso
```

> You are downloading Ubuntu (1.8go)

## Step 2: Unpack files and partition images

### Extract files

`7z -y x ubuntu-22.04.2-live-server-amd64.iso -osource-files`

### Move mbr, UEFI

`cd source-files && mv  '[BOOT]' ../BOOT && cd ../`

## Step 3: Edit the ISO grub.cfg file

Add following code to the grub.cfg file:  [grub.cfg](./u22.04-autoinstall-ISO/source-files/boot/grub.cfg)

```
menuentry "Autoinstall Ubuntu Server" {
    set gfxpayload=keep
    linux   /casper/vmlinuz quiet autoinstall ds=nocloud\;s=/cdrom/server/  ---
    initrd  /casper/initrd
}
```

## Step 4: Create data and custom autoinstall

```sh
mkdir source-files/server &&
touch source-files/server/meta-data
```

> The meta-data file is just an empty file that cloud-init expects to be present (it would be populated with data needed when using cloud services)

Edit [user-data file](./u22.04-autoinstall-ISO/source-files/user-data)

## Step 5: Generate new autoinstall ISO

```sh
cd source-files

xorriso -as mkisofs -r \
  -V 'Ubuntu 22.04 LTS AUTO (EFIBIOS)' \
  -o ../ubuntu-22.04-autoinstall.iso \
  --grub2-mbr ../BOOT/1-Boot-NoEmul.img \
  -partition_offset 16 \
  --mbr-force-bootable \
  -append_partition 2 28732ac11ff8d211ba4b00a0c93ec93b ../BOOT/2-Boot-NoEmul.img \
  -appended_part_as_gpt \
  -iso_mbr_part_type a2a0d0ebe5b9334487c068b6b72699c7 \
  -c '/boot.catalog' \
  -b '/boot/grub/i386-pc/eltorito.img' \
    -no-emul-boot -boot-load-size 4 -boot-info-table --grub2-boot-info \
  -eltorito-alt-boot \
  -e '--interval:appended_partition_2:::' \
  -no-emul-boot \
  .
```